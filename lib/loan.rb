class Loan

  LOAN_PERIOD = 36

  attr_reader :amount, :rate

  def initialize(amount, rate)
    raise ArgumentError unless (amount > 0 && rate > 0)
    @amount = amount
    @rate = rate
  end

  def total_due
    monthly_due*LOAN_PERIOD
  end

  def monthly_due
    # ref: https://en.wikipedia.org/wiki/Compound_interest
    (@amount*@rate/12) * (1/(1-((1+@rate/12)**-LOAN_PERIOD)))
  end

  def print
    puts "Requested amount: £#{amount.round(2).to_s}"
    puts "Rate: #{(rate*100).round(1).to_s}%"
    puts "Monthly repayment: £#{monthly_due.round(2).to_s}"
    puts "Total repayment: £#{total_due.round(2).to_s}"
  end

end
require 'csv'

class Market

  LENDER_NAME_COL = 0
  LENDER_RATE_COL = 1
  LENDER_AMOUNT_COL = 2

  attr_accessor :lenders # might be useful to make a Lender class

  def initialize(market_file)
    # read the csv file
    raise ArgumentError unless File.exists?(market_file)

    @lenders = CSV.read(market_file, converters: :numeric)[1..-1].sort_by {|e| [e[LENDER_RATE_COL], e[LENDER_AMOUNT_COL]]}
  end

  def available_amount
    @lenders.map {|e| e[LENDER_AMOUNT_COL]}.reduce(:+)
  end

  def check_amount(amount)
    amount <= available_amount
  end

  def find_lenders(amount)
    return nil unless check_amount(amount)
    tmp_sum = amount
    proposed_lenders = []
    @lenders.each do |l|
      if tmp_sum > 0
        lending_amount = [tmp_sum,l[LENDER_AMOUNT_COL]].min
        proposed_lenders << [l[LENDER_NAME_COL], l[LENDER_RATE_COL], lending_amount]
        tmp_sum -= lending_amount
        # p "#{l[LENDER_NAME_COL]},: #{l[LENDER_AMOUNT_COL] - lending_amount}, remainder: #{tmp_sum.to_s}"
      else
        break
      end
    end
    proposed_lenders
  end

  def get_rate(lenders)
    amount = lenders.map {|l| l[LENDER_AMOUNT_COL]}.reduce(:+)
    lenders.map {|l| l[LENDER_AMOUNT_COL]*l[LENDER_RATE_COL]}.reduce(:+) / amount
  end

  def make_loan(amount)
    proposed_lenders = find_lenders(amount)
    return nil if proposed_lenders.nil?
    rate = get_rate(proposed_lenders)
    Loan.new(amount, rate)
  end
end
#!/usr/bin/env ruby

require './lib/market'
require './lib/loan'

def print_usage
  puts "Usage: load.rb MARKET_FILE LOAN_AMOUNT"
  exit
end

# arguments validation
valid_arguments = true
if ARGV.size() < 2
  puts "Error: Too few arguments"
  print_usage
end

if ARGV.size() > 2
  puts "Error: Too many arguments"
  print_usage
end

market_file_path = ARGV[0]
if !File.exists?(market_file_path)
  puts "Error: The file doesn't exist"
  print_usage
end

if ARGV[1].to_i.to_s != ARGV[1]
  puts "Error: The amount has to be an integer number"
  print_usage
end

loan_amount = ARGV[1].to_i

if (loan_amount <= 0) || (loan_amount % 100 != 0)
  puts "Error: The amount has to be positive and multiple of 100"
  print_usage
end

begin
  market = Market.new(market_file_path)
  loan = market.make_loan(loan_amount)
rescue ArgumentError
  puts "There was an error."
  print_usage
end

if loan.nil?
  puts "Error: The amount is greater than the market capacity"
else
  loan.print
end
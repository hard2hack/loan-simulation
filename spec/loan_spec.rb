require 'loan'

RSpec.describe Loan do

  let(:amount_ok) {1200}
  let(:rate_ok) {0.07055}
  let(:monthly_due_ok) {37.08}
  let(:total_due_ok) {1334.98}

  let(:amount_null) {0}
  let(:rate_null) {0}

  context "#new" do
    it "creates a Loan object" do
      loan = Loan.new(amount_ok, rate_ok)
      expect(loan).to be_an_instance_of(Loan)
    end

    it "raise an exception if the amount is not positive" do
      expect { Loan.new(amount_null, rate_ok) }.to raise_error(ArgumentError)
    end

    it "raise an exception if the rate is not positive" do
      expect { Loan.new(amount_ok, rate_null) }.to raise_error(ArgumentError)
    end

    it "populates the amount variable to with the right value" do
      loan = Loan.new(amount_ok, rate_ok)
      expect(loan.amount).to eq(amount_ok)
    end

    it "populates the rate variable to with the right value" do
      loan = Loan.new(amount_ok, rate_ok)
      expect(loan.rate).to eq(rate_ok)
    end
  end

  context "#monthly_due" do
    it "returns the correct monthly due amount" do
      loan = Loan.new(amount_ok, rate_ok)
      expect(loan.monthly_due.round(2)).to eq(monthly_due_ok)
    end
  end

  context "#total_due" do
    it "returns the correct total due amount" do
      loan = Loan.new(amount_ok, rate_ok)
      expect(loan.total_due.round(2)).to eq(total_due_ok)
    end
  end
end
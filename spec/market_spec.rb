require 'market'
require 'loan'

RSpec.describe Market do

  let(:lenders) {[
                  ["Bob",0.075,640],
                  ["Jane",0.069,480],
                  ["Fred",0.071,520],
                  ["Mary",0.104,170],
                  ["John",0.081,320],
                  ["Dave",0.074,140],
                  ["Angela",0.071,60]
                ]}

  let(:ordered_lenders) {[
                  ["Jane",0.069,480],
                  ["Angela",0.071,60],
                  ["Fred",0.071,520],
                  ["Dave",0.074,140],
                  ["Bob",0.075,640],
                  ["John",0.081,320],
                  ["Mary",0.104,170]
                ]}

  let(:available_amount){2330}
  let(:requested_amount_ok){1200}
  let(:right_lenders) {[
                  ["Jane",0.069,480],
                  ["Angela",0.071,60],
                  ["Fred",0.071,520],
                  ["Dave",0.074,140]
                ]}
  let(:right_rate) {0.07055}
  let(:right_total_due) {1334.98}
  let(:right_monthly_due) {37.08}

  let(:requested_amount_ok_2){1400}
  let(:right_lenders_2) {[
                  ["Jane",0.069,480],
                  ["Angela",0.071,60],
                  ["Fred",0.071,520],
                  ["Dave",0.074,140],
                  ["Bob",0.075,200]
                ]}
  let(:right_rate_2) {0.07119}

  let(:requested_amount_ko){3600}

  context "#new" do
    it "raise an exception if the market file doesn't exist" do
      expect { Market.new("wrong_file.csv") }.to raise_error(ArgumentError)
    end

    it "creates a Market object" do
      market = Market.new("market.csv")
      expect(market).to be_an_instance_of(Market)
    end

    it "populates the lenders variable to with the right values" do
      market = Market.new("market.csv")
      expect(market.lenders).to eq(ordered_lenders)
    end
  end

  context "#available_amount" do
    it "returns the available amount of the market" do
      market = Market.new("market.csv")
      expect(market.available_amount).to eq(available_amount)
    end
  end

  context "#check_amount" do
    before(:each) do
      @market = Market.new("market.csv")
    end
    it "returns true if the available amount is greater of the requested amount" do
      expect(@market.check_amount(requested_amount_ok)).to be true
    end
    it "returns true if the available amount is equal to the requested amount" do
      expect(@market.check_amount(available_amount)).to be true
    end
    it "returns false if the available amount is lesser of the requested amount" do
      expect(@market.check_amount(requested_amount_ko)).to be false
    end
  end

  context "#find_lenders" do
    before(:each) do
      @market = Market.new("market.csv")
    end

    it "returns nil if the amount requested is greater than the available amount" do
      lenders = @market.find_lenders(requested_amount_ko)
      expect(lenders).to be nil
    end

    context "finds the right lenders for a requested amount" do
      it "that's even with the sum of the lenders available amounts" do
        lenders = @market.find_lenders(requested_amount_ok)
        expect(lenders).to eq(right_lenders)
      end

      it "that is not even with the sum of the lenders available amounts" do
        lenders = @market.find_lenders(requested_amount_ok_2)
        expect(lenders).to eq(right_lenders_2)
      end
    end
  end

  context "#get_rate" do
    market = Market.new("market.csv")
    it "calculates the right rate for the loan" do
      proposed_lenders = market.find_lenders(requested_amount_ok)
      expect(market.get_rate(proposed_lenders)).to eq(right_rate)
    end
  end

  context "#make_loan" do
    before(:each) do
      @market = Market.new("market.csv")
    end

    it "fails when requesting more than the available amount" do
      expect(@market.make_loan(requested_amount_ko)).to be nil
    end

    context "returns a Loan object " do
      before do
        @loan = @market.make_loan(requested_amount_ok)
      end

      it { expect(@loan).to be_an_instance_of(Loan) }

      it "with the right amount value" do
        expect(@loan.amount).to eq(requested_amount_ok)
      end

      it "with the right rate value" do
        expect(@loan.rate).to eq(right_rate)
      end

      it "with the right total due value" do
        expect(@loan.total_due.round(2)).to eq(right_total_due)
      end

      it "with the right monthly due value" do
        expect(@loan.monthly_due.round(2)).to eq(right_monthly_due)
      end
    end
  end
end